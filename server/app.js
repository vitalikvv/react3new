import {users, messages} from "./database/database.js"
import express from 'express'
import bodyParser from 'body-parser'
import {updateUser, createUser, deleteUser, getAuthData, createMessage, updateMessage, deleteMessage} from "./database/service.js";

const app = express();
const jsonParser = bodyParser.json()
const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`Listening on port ${port}`));

app.post("/auth", jsonParser, function(req, res){
    const user = getAuthData(req.body)
    if (user) {
        res.send(user);
    } else {
        res.status(400).json({
            error: true,
            message: 'User data not found, enter: admin/admin'
        });
    }
});

app.get("/messages", function(req, res){
    res.send(messages)
});

app.post("/messages", jsonParser, function(req, res){
    createMessage(req.body);
    res.send('Message added')
});

app.put("/messages", jsonParser, function(req, res){
    updateMessage(req.body);
    res.send('Message updated')
});

app.delete("/messages/:id", function(req, res){
    deleteMessage(req.params.id)
    res.send('Message deleted')
});

app.get("/users", function(req, res){
    res.send(users);
});

app.put("/users", jsonParser, function(req, res){
    if (req.body.userId) {
        updateUser(req.body);
    } else {
        createUser(req.body)
    }
    res.send('User added')
});

app.delete("/users/:id", function(req, res){
    deleteUser(req.params.id)
    res.send('User deleted')
});
