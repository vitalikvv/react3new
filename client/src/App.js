import React from 'react';
import Chat from "./Chat";
import './App.css'
import {Redirect, Route, Switch} from "react-router-dom";
import MainHeader from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import MessageEditor from "./components/MessageEditor/MessageEditor";
import LoginForm from "./components/LoginForm/LoginForm";
import Toast from "react-toast-component";
import {useDispatch, useSelector} from "react-redux";
import {displayErrorMessage, setLikeOrDis} from "./store/messagesList";
import UsersList from "./components/UsersList/UsersList";
import Preloader from "./components/Preloader/Preloader";
import UserEditor from "./components/UserEditor/UserEditor";

function App() {
    const {likeOrDisValue, startLoading, errorMessage, currentUser} = useSelector(state => ({
        errorMessage: state.chat.errorMessage,
        currentUser: state.chat.currentUser,
        startLoading: state.chat.startLoading,
        likeOrDisValue: state.chat.likeOrDisValue,
    }));
    const dispatch = useDispatch();

    if (!currentUser.role) {
        return <>
            <main>
                <LoginForm/>
            </main>
            <Toast
                isOpen={errorMessage}
                hasAutoDismiss={true}
                closeCallback={() => dispatch(displayErrorMessage(null))}
                description={errorMessage}
                title="Notification!"
                duration={5000}
                classNames={['error', 'notification-error']}
            />
        </>
    } else {

        return (
            <>
                {currentUser.role === 'admin' ? <Redirect to='/users_list'/> : <Redirect to='/chat'/>}
                {startLoading ? <Preloader/> : null}
                <MainHeader/>
                <main>
                    <Switch>
                        <Route path="/" exact render={currentUser.role === 'admin'
                            ? () => <Redirect to='/users_list'/>
                            : () => <Redirect to='/chat'/>}
                        />
                        <Route path="/chat" render={() => <Chat/>}/>
                        <Route path="/message_editor" render={() => <MessageEditor/>}/>
                        <Route path="/users_list" render={() => <UsersList/>}/>
                        <Route path="/user_editor" render={() => <UserEditor/>}/>
                        <Route path="*" exact render={() =>
                            <div style={{
                                marginTop: '90px',
                                flex: '1 0 auto',
                                display: 'flex',
                                justifyContent: 'center',
                                alignItems: 'center',
                                fontSize: '30px'
                            }}
                            >NOT FOUND</div>
                        }/>
                    </Switch>
                </main>
                <Footer/>
                <Toast
                    isOpen={errorMessage || likeOrDisValue}
                    hasAutoDismiss={true}
                    closeCallback={() => {
                        dispatch(displayErrorMessage(null))
                        dispatch(setLikeOrDis(null))
                    }}
                    description={errorMessage
                        ? errorMessage
                        : likeOrDisValue === 'like' ? 'You liked post.' : 'You disliked post.'}
                    title="Notification!"
                    duration={3000}
                    classNames={errorMessage
                        ? ['error', 'notification-error']
                        : likeOrDisValue === 'like'
                            ? ['success', 'notification-message-like']
                            : ['error', 'notification-message-dislike']
                    }
                />
            </>
        );
    }
}

export default App;
