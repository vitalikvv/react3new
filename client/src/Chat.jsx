import React, {useEffect} from 'react'
import MessageList from "./components/MessageList/MessageList";
import {useDispatch} from "react-redux";
import {
    getAllMessages,
} from "./store/messagesList";

const Chat = () => {
    const dispatch = useDispatch();
    const getMessages = React.useCallback(() => (
        dispatch(getAllMessages())
    ), [dispatch]);

    useEffect( () => {
        getMessages();
        localStorage.clear();
    }, [getMessages])

    return (
        <div className='chat'>
            <MessageList />
        </div>
    )
}

export default Chat