import React, {useState} from 'react'
import {Button, Container, Form, TextArea} from "semantic-ui-react";
import './MessageInput.css'

const MessageInput = ({addNewMessage, currentUser}) => {
    const [value, setValue] = useState('')

    const changeHandler = (e) => {
        setValue(e.target.value)
    }

    const clickHandler = () => {
        if (value.length > 0) {
            addNewMessage(value, currentUser)
            setValue('')
        }
    }

    return (
        <div className='message-input'>
            <Container style={{width: '1200px'}}>
                <Form style={{marginBottom: '20px'}}>
                    <TextArea
                        value={value}
                        onChange={changeHandler}
                        as='textarea'
                        rows={3}
                        className='message-input-text'
                        label='Input your message'
                        placeholder='Tell us something...'
                    />
                </Form>
                <Button
                    className='message-input-button'
                    onClick={clickHandler}
                >
                    Submit
                </Button>
            </Container>
        </div>
    )
}

export default MessageInput