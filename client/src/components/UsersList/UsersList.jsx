import React, {useEffect, useState} from 'react'
import {Button, Container, List, Segment} from "semantic-ui-react";
import './UsersList.css'
import {useDispatch, useSelector} from "react-redux";
import {deleteUser, getAllUsers, setUserToEdit} from "../../store/messagesList";
import User from "../User/User";
import {Redirect} from "react-router-dom";

const UsersList = () => {

    const {users, currentUser} = useSelector(state => ({
        users: state.chat.users,
        currentUser: state.chat.currentUser
    }));

    const dispatch = useDispatch();
    const [redirectData, setRedirectData] = useState(null)
    useEffect(() => {
        dispatch(getAllUsers());
    }, [dispatch])

    const goToUserEditor = (user) => {
        setRedirectData(user);
        dispatch(setUserToEdit(user))
    }

    const handleDeleteUser = (userId) => {
        dispatch(deleteUser(userId))
    }

    return (
        <>
            {redirectData && <Redirect
                to="/user_editor"
            />}
            <div className='users-list'>
                <Container style={{width: '700px'}}>
                    <Segment>
                        <Button onClick={() => goToUserEditor({})} color='teal'>Add USER</Button>
                        <List divided verticalAlign='middle'>
                            {users.map(user =>
                                <User
                                    key={user.userId}
                                    goToUserEditor={goToUserEditor}
                                    deleteUser={handleDeleteUser}
                                    user={user}
                                    currentUser={currentUser}
                                />
                            )}
                        </List>
                    </Segment>
                </Container>
            </div>
        </>
    )
}

export default UsersList