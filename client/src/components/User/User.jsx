import React from 'react'
import {Button, Icon, Image, List} from "semantic-ui-react";
import './User.css'


const User = ({goToUserEditor, deleteUser, user, currentUser}) => {

    return (
        <List.Item>
            <List.Content floated='right'>
                {user.userId === currentUser.userId
                    ? <div className={'you-wrapper'}><Icon name='lock'/>You</div>
                    : <div>
                        <Button onClick={() => goToUserEditor(user)}>Edit</Button>
                        <Button onClick={() => deleteUser(user.userId)}>Delete</Button>
                    </div>}
            </List.Content>
            <Image avatar src={user.avatar}/>
            <List.Content>
                <List.Header>{user.user}</List.Header>
                {user.role}
            </List.Content>
        </List.Item>
    )
}

export default User;