import React, {useState} from 'react'
import {Container, Form, Image, Segment} from "semantic-ui-react";
import './UserEditor.css'
import {useDispatch, useSelector} from "react-redux";
import {getRandomFoto} from "../../helpers/getCurrentUser";
import {Redirect} from "react-router-dom";
import {displayErrorMessage, userDataCreateOrUpdate} from "../../store/messagesList";

const UserEditor = () => {
    const {userToEdit} = useSelector(state => ({
        userToEdit: state.chat.userToEdit,
    }));
    const dispatch = useDispatch();
    const [radioValue, setRadioValue] = useState(Object.keys(userToEdit).length > 0 ? userToEdit.role : null)
    const [fotoAction, setFotoAction] = useState(null)
    const [avatarLink, setAvatarLink] = useState(Object.keys(userToEdit).length > 0 ? userToEdit.avatar : 'https://react.semantic-ui.com/images/wireframe/image.png')
    const [userName, setUserName] = useState(Object.keys(userToEdit).length > 0 ? userToEdit.user : '')
    const [password, setPassword] = useState(Object.keys(userToEdit).length > 0 ? userToEdit.password : '')
    const [isRedirectToUserList, setIsRedirectToUserList] = useState(false)

    const options = [
        { key: 'ra', text: 'get random', value: 'random' },
        { key: 'no', text: 'dont change', value: 'noChange' },
    ]

    const changeUserName = (e) => {
        setUserName(e.target.value)
    }

    const changePassword = (e) => {
        setPassword(e.target.value)
    }

    const handleChange = (e, { value }) => setRadioValue(value)
    const updateFotoAction = async (e, { value }) => {
        setFotoAction(value)
        if (value === 'random') {
            const randomFoto = await getRandomFoto();
            setAvatarLink(randomFoto);
        }
    }

    const createOrUpdateUser = () => {
        if (!radioValue) {
            dispatch(displayErrorMessage('Select user role'))
            return
        }
        if (!userName) {
            dispatch(displayErrorMessage('Enter user name'))
            return
        }
        if (!password) {
            dispatch(displayErrorMessage('Enter user password'))
            return
        }
        const request = {
            ...userToEdit,
            role: radioValue,
            user: userName,
            password,
            avatar: avatarLink
        }
        dispatch(userDataCreateOrUpdate(request))
        setIsRedirectToUserList(true)
    }

    return (
        <>
            {isRedirectToUserList && <Redirect from='/user_editor' to='/users_list'/>}
            <div className='user-editor'>
                <Container style={{width: '700px'}}>
                    <Segment>
                        <Form>
                            <Image
                                src={avatarLink}
                                size='small'
                                floated='left'
                            />
                            <Form.Group widths='equal'>
                                <Form.Input
                                    fluid
                                    label='User'
                                    placeholder='User name'
                                    onChange={changeUserName}
                                    value={userName}
                                />
                                <Form.Input
                                    fluid
                                    label='Password'
                                    placeholder='Password'
                                    onChange={changePassword}
                                    value={password}
                                />
                                <Form.Select
                                    fluid
                                    label='Actions with avatar'
                                    options={options}
                                    placeholder='Select action'
                                    onChange={updateFotoAction}
                                    value={fotoAction}
                                />
                            </Form.Group>
                            <Form.Group inline>
                                <Form.Radio
                                    label='Admin'
                                    value='admin'
                                    checked={radioValue === 'admin'}
                                    onChange={handleChange}
                                />
                                <Form.Radio
                                    label='User'
                                    value='user'
                                    checked={radioValue === 'user'}
                                    onChange={handleChange}
                                />
                            </Form.Group>
                            <Form.Button onClick={createOrUpdateUser}>Submit</Form.Button>
                        </Form>
                    </Segment>
                </Container>
            </div>
        </>
    )
}

export default UserEditor