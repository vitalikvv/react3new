import React, {useState} from 'react'
import {Button, Form, Image, Modal, TextArea} from 'semantic-ui-react'
import './Modal.css'

const ModalWindow = ({messageId, text, avatar, updateMessage, close}) => {
    const [open, setOpen] = React.useState(true)
    const [newMessage, setNewMessage] = useState(text);

    React.useEffect(() => {
        if (!open) {
            close();
        }
    }, [open]);

    const changeMessage = (e) => {
        setNewMessage(e.target.value)
    }

    const handleUpdateMessage = () => {
        updateMessage(newMessage, messageId)
    }

    return (
        <Modal
            onClose={() => setOpen(false)}
            open={open}
            className={open ? 'edit-message-modal modal-shown' : 'edit-message-modal'}
        >
            <Modal.Header>Edit message</Modal.Header>
            <Modal.Content image>
                <Image size='medium' src={avatar} wrapped />
                <Modal.Description style={{width: '100%'}}>
                    <Form>
                        <TextArea
                            className='edit-message-input'
                            as={'textarea'}
                            autoFocus={true}
                            type="text"
                            onChange={changeMessage}
                            defaultValue={newMessage}
                        />
                    </Form>
                </Modal.Description>
            </Modal.Content>
            <Modal.Actions>
                <Button
                    className='edit-message-close'
                    color='black'
                    onClick={() => setOpen(false)}
                >
                    Close
                </Button>
                <Button
                    className='edit-message-button'
                    content="Send"
                    labelPosition='right'
                    icon='checkmark'
                    onClick={() => {
                        handleUpdateMessage();
                        setOpen(false)
                    }}
                    positive
                />
            </Modal.Actions>
        </Modal>
    )
}

export default ModalWindow
