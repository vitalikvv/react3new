import * as React from 'react';
import {Form, Segment} from "semantic-ui-react";
import './LoginForm.css'
import {useDispatch, useSelector} from "react-redux";
import {setAuth} from "../../store/messagesList";

const LoginForm = () => {
    const { isLoadingLoginForm } = useSelector(state => ({
        isLoadingLoginForm: state.chat.isLoadingLoginForm,
    }));

    const [login, setLogin] = React.useState('');
    const [password, setPassword] = React.useState('');
    const dispatch = useDispatch();

    const loginChanged = data => {
        setLogin(data);
    };

    const passwordChanged = data => {
        setPassword(data);
    };

    const handleLoginClick = () => {
        if (isLoadingLoginForm) {
            return;
        }
        dispatch(setAuth({login, password}))
    };

    return (
        <>
            <div className='login-form'>
                <h2>Login to your account</h2>
                <Form name="loginForm" size="large" onSubmit={handleLoginClick} style={{width: '350px'}}>
                    <Segment>
                        <Form.Input
                            fluid
                            icon="user"
                            iconPosition="left"
                            placeholder="Login"
                            type="text"
                            onChange={ev => loginChanged(ev.target.value)}
                        />
                        <Form.Input
                            fluid
                            icon="lock"
                            iconPosition="left"
                            placeholder="Password"
                            type="password"
                            onChange={ev => passwordChanged(ev.target.value)}
                        />
                        <Form.Button
                            type='submit'
                            color='teal'
                            size='medium'
                            loading={isLoadingLoginForm}
                        >
                            Login
                        </Form.Button>
                    </Segment>
                </Form>

            </div>
        </>
    );
};

export default LoginForm;
