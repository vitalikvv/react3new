import React from 'react'
import './Header.css'
import {Icon, Container, Header} from "semantic-ui-react";
import HeaderMenu from "./Menu/HeaderMenu";
import {Link} from "react-router-dom";


const MainHeader = () => {

    return (
        <div className='header my-header'>
            <Container className='header-container'>
                <Link to="/" style={{display: 'block', height: '100%'}}>
                    <div className='header-title'>
                        <Header as='h2'>
                            <Icon name='plug'/>
                            <Header.Content style={{whiteSpace: 'nowrap'}}>
                                My chat
                            </Header.Content>
                        </Header>
                    </div>
                </Link>
                <HeaderMenu/>
            </Container>
        </div>
    )
}

export default MainHeader