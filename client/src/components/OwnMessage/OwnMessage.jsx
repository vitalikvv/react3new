import React, {useEffect, useState} from 'react'
import {Comment, Icon} from "semantic-ui-react";
import './OwnMessage.css'
import {Redirect} from "react-router-dom";
import { useCallback } from 'react'

const OwnMessage = ({message, likeMessage, setLikeOrDis, deleteMessage, isLastElement}) => {
    const [isRedirectToMessageEditor, setIsRedirectToMessageEditor] = useState(false)

    const handleUserKeyPress = useCallback((e) => {
        if (e.key === 'ArrowUp' && isLastElement) {
            setIsRedirectToMessageEditor(true)
        }
    }, [isLastElement])

    useEffect(() => {
        window.addEventListener('keydown', handleUserKeyPress);
        return () => {
            window.removeEventListener('keydown', handleUserKeyPress);
        };
    }, [handleUserKeyPress]);

    const clickLikeHandler = () => {

        if (localStorage.getItem(message.id)) {
            setLikeOrDis('dislike');
            likeMessage(message.id, 'dislike');
            return
        }
        setLikeOrDis('like')
        likeMessage(message.id, 'like');
    }

    const clickDeleteHandler = () => {
        deleteMessage(message.id);
    }

    return (
        <>
            {isRedirectToMessageEditor && <Redirect from='/chat' to='/message_editor'/>}
            <div className='own-message'>
                <Comment.Group>
                    <Comment>
                        <Comment.Avatar as='a' className='message-user-avatar' src={message.avatar}/>
                        <Comment.Content>
                            <Comment.Author className='message-user-name'>{message.user}</Comment.Author>
                            <Comment.Text className='message-text'>
                                {message.text}
                            </Comment.Text>
                            <Comment.Actions>
                                <Comment.Action
                                    style={{marginRight: 0}}
                                    onClick={clickLikeHandler}
                                    className={message.likesCount > 0 ? 'message-liked' : 'message-like'}><Icon
                                    name='like'/>
                                </Comment.Action>
                                <Comment.Metadata style={{width: '10px', marginLeft: 0, marginRight: '5px'}}>
                                    <span>{message.likesCount > 0 ? message.likesCount : null}</span>
                                </Comment.Metadata>
                                <Comment.Action onClick={() => setIsRedirectToMessageEditor(true)}>
                                    {isLastElement && <Icon name='edit'/>}
                                </Comment.Action>
                                <Comment.Action
                                    className='message-delete'
                                    onClick={clickDeleteHandler}
                                >
                                    <Icon name='delete'/>
                                </Comment.Action>
                            </Comment.Actions>
                            <Comment.Metadata style={{marginLeft: 0}}>
                                <div className='message-time'>{message.createdAt.split(' ')[1]}</div>
                            </Comment.Metadata>
                        </Comment.Content>
                    </Comment>
                </Comment.Group>
            </div>
        </>
    )
}

export default OwnMessage;