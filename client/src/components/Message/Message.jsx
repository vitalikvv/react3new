import React from 'react'
import {Comment, Icon} from "semantic-ui-react";
import './Message.css'


const Message = ({message, likeMessage, setLikeOrDis }) => {

    const clickHandler = () => {

        if (localStorage.getItem(message.id)) {
            setLikeOrDis('dislike');
            likeMessage(message.id, 'dislike');
            return
        }
        setLikeOrDis('like')
        likeMessage(message.id, 'like');
    }

    return (
        <div className='message'>
            <Comment.Group>
                <Comment>
                    <Comment.Avatar as='a' className='message-user-avatar' src={message.avatar} />
                    <Comment.Content>
                        <Comment.Author className='message-user-name'>{message.user}</Comment.Author>
                        <Comment.Text className='message-text'>
                            {message.text}
                        </Comment.Text>
                        <Comment.Actions>
                            <Comment.Action
                                style={{marginRight: 0}}
                                onClick={clickHandler}
                                className={message.likesCount > 0 ? 'message-liked' : 'message-like'}
                            >
                                <Icon name='like' /></Comment.Action>
                            <Comment.Metadata style={{ width: '10px', marginLeft:0, marginRight: '5px' }}>
                                <span>{message.likesCount > 0 ? message.likesCount : null}</span>
                            </Comment.Metadata>
                        </Comment.Actions>
                        <Comment.Metadata style={{ marginLeft:0 }}>
                            <div className='message-time'>{message.createdAt.split(' ')[1]}</div>
                        </Comment.Metadata>
                    </Comment.Content>
                </Comment>
            </Comment.Group>
        </div>
    )
}

export default Message;