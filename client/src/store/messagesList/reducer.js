import {messagesListActionTypes} from "./actions";
import {createReducer} from "@reduxjs/toolkit";

const initialState ={
    messages: [],
    currentUser: {},
    isOpenModal: false,
    startLoading: false,
    likeOrDisValue: '',
    isOpenToast: false,
    ownLastMessage: {},
    isLoadingLoginForm: false,
    errorMessage: null,
    users: [],
    userToEdit: {}
}

export const chat = createReducer(initialState, builder => {
    builder.addCase(messagesListActionTypes.SET_MESSAGES, (state, action) => {
        const { messages } = action.payload;
        state.messages = messages;
    });
    builder.addCase(messagesListActionTypes.SET_CURRENT_USER, (state, action) => {
        const { currentUser } = action.payload;

        state.currentUser = currentUser;
    });
    builder.addCase(messagesListActionTypes.ADD_MESSAGE, (state, action) => {
        const { message } = action.payload;

        state.messages = [...state.messages, message];
    });
    builder.addCase(messagesListActionTypes.LIKE_MESSAGE, (state, action) => {
        const {messageId, likeOrDis} = action.payload;
        state.messages = state.messages.map(message => {
            if (message.id === messageId) {
                likeOrDis === 'like' ? localStorage.setItem(messageId, messageId) : localStorage.removeItem(messageId)
                return {
                    ...message,
                    likesCount: likeOrDis === 'like' ? message.likesCount + 1 : message.likesCount - 1
                };
            }
            return message;
        });
    });
    builder.addCase(messagesListActionTypes.SET_START_LOADING, (state, action) => {

        state.startLoading = action.payload.isLoading;
    });
    builder.addCase(messagesListActionTypes.SET_LIKE_OR_DIS, (state, action) => {

        state.likeOrDisValue = action.payload.likeOrDis;
    });
    builder.addCase(messagesListActionTypes.SET_OWN_LAST_MESSAGE, (state, action) => {

        state.ownLastMessage = action.payload.message;
    });
    builder.addCase(messagesListActionTypes.SET_IS_LOADING_LOGIN_FORM, (state, action) => {

        state.isLoadingLoginForm = action.payload.isLoading;
    });
    builder.addCase(messagesListActionTypes.SET_IS_ERROR_MESSAGE, (state, action) => {

        state.errorMessage = action.payload.errorMessage;
    });
    builder.addCase(messagesListActionTypes.SET_USERS_LIST, (state, action) => {

        state.users = action.payload.users;
    });
    builder.addCase(messagesListActionTypes.SET_USER_TO_EDIT, (state, action) => {

        state.userToEdit = action.payload.user;
    });
});