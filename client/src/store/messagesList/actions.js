import {changeDateFormat} from "../../helpers/changeDateFormat";
import {v4 as uuidv4} from "uuid";
import {
    deleteUserById,
    getAuthUser,
    getMessages,
    getUsers,
    updateOrCreateUser,
    newMessage,
    updateMessageById, deleteMessageById
} from "../../services/apiService";
import {createAction} from "@reduxjs/toolkit";

export const messagesListActionTypes = {
    SET_MESSAGES: "MESSAGESLIST.SET_MESSAGES",
    SET_CURRENT_USER: "MESSAGESLIST.SET_CURRENT_USER",
    ADD_MESSAGE: "MESSAGESLIST.ADD_MESSAGE",
    LIKE_MESSAGE: "MESSAGESLIST.LIKE_MESSAGE",
    DELETE_MESSAGE: "MESSAGESLIST.DELETE_MESSAGE",
    SET_START_LOADING: "MESSAGESLIST.SET_START_LOADING",
    SET_LIKE_OR_DIS: "MESSAGESLIST.LIKE_OR_DIS",
    SET_OWN_LAST_MESSAGE: "MESSAGESLIST.SET_OWN_LAST_MESSAGE",
    SET_IS_LOADING_LOGIN_FORM: "MESSAGESLIST.SET_IS_LOADING_LOGIN_FORM",
    SET_IS_ERROR_MESSAGE: "MESSAGESLIST.SET_IS_ERROR_MESSAGE",
    SET_USERS_LIST: "MESSAGESLIST.SET_USERS_LIST",
    SET_USER_TO_EDIT: "MESSAGESLIST.USER_TO_EDIT",
}

export const setAuth = (payload) => async (dispatch) => {
    try {
        dispatch(setIsLoadingLoginForm(true))
        const response = await getAuthUser(payload);
        dispatch(setCurrentUser(response.data));
    } catch (e) {
        dispatch(displayErrorMessage(e))
    } finally {
        dispatch(setIsLoadingLoginForm(false))
    }
}

export const getAllUsers = () => async (dispatch) => {
    try {
        dispatch(setStartLoading(true));
        const response = await getUsers();
        dispatch(setUsers(response.data));
    } catch (e) {
        dispatch(displayErrorMessage(e.message))
    } finally {
        dispatch(setStartLoading(false));
    }
}

export const userDataCreateOrUpdate = (userData) => async (dispatch) => {
    try {
        dispatch(setStartLoading(true));
        await updateOrCreateUser(userData);
        const response = await getUsers();
        dispatch(setUsers(response.data));
    } catch (e) {
        dispatch(displayErrorMessage(e.message))
    } finally {
        dispatch(setStartLoading(false));
    }
}

export const deleteUser = (userId) => async (dispatch) => {
    try {
        dispatch(setStartLoading(true));
        await deleteUserById(userId);
        const response = await getUsers();
        dispatch(setUsers(response.data));
    } catch (e) {
        dispatch(displayErrorMessage(e.message))
    } finally {
        dispatch(setStartLoading(false));
    }
}

export const getAllMessages = () => async (dispatch) => {
    try {
        dispatch(setStartLoading(true));
        const response = await getMessages();
        const dataWithChangedDateFormat = changeDateFormat(response.data);
        dispatch(setMessages(dataWithChangedDateFormat));
        dispatch(setLastMessage());
    } catch (e) {
        dispatch(displayErrorMessage(e.message))
    } finally {
        dispatch(setStartLoading(false));
    }
}

export const addNewMessage = (text, currentUser) => async (dispatch) => {
    try {
        const {userId, avatar, user} = currentUser;
        const tzoffset = (new Date()).getTimezoneOffset() * 60000;
        const createdDate = (new Date(Date.now() - tzoffset)).toISOString();
        const message = {
            text: text,
            editedAt: '',
            id: uuidv4(),
            createdAt: createdDate,
            likesCount: 0,
            userId,
            avatar,
            user
        }
        dispatch(setStartLoading(true));
        await newMessage(message);
        const response = await getMessages();
        const dataWithChangedDateFormat = changeDateFormat(response.data);
        dispatch(setMessages(dataWithChangedDateFormat));
        dispatch(setLastMessage());
    } catch (e) {
        dispatch(displayErrorMessage(e.message))
    } finally {
        dispatch(setStartLoading(false));
    }
}

export const updateMessage = (newMessageText, messageId) => async (dispatch) => {
    try {
        const now = new Date();
        const editedDate = now.toISOString()
        dispatch(setStartLoading(true));
        await updateMessageById({newMessageText, messageId, editedDate});
        const response = await getMessages();
        const dataWithChangedDateFormat = changeDateFormat(response.data);
        dispatch(setMessages(dataWithChangedDateFormat));
        dispatch(setLastMessage());
    } catch (e) {
        dispatch(displayErrorMessage(e.message))
    } finally {
        dispatch(setStartLoading(false));
    }
}

export const deleteMessage = (messageId) => async (dispatch) => {
    try {
        dispatch(setStartLoading(true));
        await deleteMessageById(messageId);
        const response = await getMessages();
        const dataWithChangedDateFormat = changeDateFormat(response.data);
        dispatch(setMessages(dataWithChangedDateFormat));
        dispatch(setLastMessage());
    } catch (e) {
        dispatch(displayErrorMessage(e.message))
    } finally {
        dispatch(setStartLoading(false));
    }
}

export const setLastMessage = () => (dispatch, getState) => {
    const {
        chat: { messages, currentUser }
    } = getState();

    const messagesFromCurrentUser = messages.filter(message => {
        return message.userId === currentUser.userId
    })

    messagesFromCurrentUser.sort(function(a,b){
        const c = new Date(a.createdAt).valueOf();
        const d = new Date(b.createdAt).valueOf();
        return c-d;
    });

    dispatch(setOwnLastMessage(messagesFromCurrentUser[messagesFromCurrentUser.length-1]));
}

export const setMessages = createAction(messagesListActionTypes.SET_MESSAGES, messages => ({
    payload: {
        messages
    }
}));

export const setCurrentUser = createAction(messagesListActionTypes.SET_CURRENT_USER, currentUser => {
    return ({
        payload: {
            currentUser
        }
    })
});

export const likeMessage = createAction(messagesListActionTypes.LIKE_MESSAGE, (messageId, likeOrDis) => ({
    payload: {
        messageId,
        likeOrDis
    }
}));

export const setStartLoading = createAction(messagesListActionTypes.SET_START_LOADING, isLoading => ({
    payload: {
        isLoading
    }
}));

export const setLikeOrDis = createAction(messagesListActionTypes.SET_LIKE_OR_DIS, likeOrDis => ({
    payload: {
        likeOrDis
    }
}));

export const setOwnLastMessage = createAction(messagesListActionTypes.SET_OWN_LAST_MESSAGE, message => ({
    payload: {
        message
    }
}));

export const setIsLoadingLoginForm = createAction(messagesListActionTypes.SET_IS_LOADING_LOGIN_FORM, isLoading => ({
    payload: {
        isLoading
    }
}));

export const displayErrorMessage = createAction(messagesListActionTypes.SET_IS_ERROR_MESSAGE, errorMessage => ({
    payload: {
        errorMessage
    }
}));

export const setUsers = createAction(messagesListActionTypes.SET_USERS_LIST, users => ({
    payload: {
        users
    }
}));

export const setUserToEdit = createAction(messagesListActionTypes.SET_USER_TO_EDIT, user => ({
    payload: {
        user
    }
}));

